from kivymd.app import MDApp
from kivy.lang import Builder

class MyApp(MDApp):
    
    def build(self):
        self.theme_cls.material_style = "M3"
        self.theme_cls.theme_style = "Light" #"Dark"
        self.theme_cls.primary_palette = "Gray"
        #self.theme_cls.accent_palette = "Red"
        return Builder.load_file("Main.kv")

    def on_start(self):
        pass    


if __name__ == "__main__":
    MyApp().run()